<?php
session_start();
if (!isset($_SESSION['nik'])) {
	header("Location: login.php");
}

$arr = file_get_contents("perjalanan.txt");
$catatan = json_decode($arr,true);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Peduli Diri</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<div class="head">
			<h1>Peduli Diri</h1>
			<p>Catatan Perjalanan</p>
		</div>
		<div class="nav">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="catatan.php">Catatan Perjalanan</a></li>
				<li><a href="input.php">Isi Data</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</div>
		<div class="urut">
			<p>Urutkan berdasarkan 
				<select>
					<option>Tanggal</option>
					<option>Jam</option>
					<option>Suhu</option>
				</select>
				<button>urutkan</button>
			</p>
		</div>
		<div class="table tbl-catatan">
			<table border="1">
				<tr>
					<th>Tanggal</th>
					<th>Jam</th>
					<th>Lokasi</th>
					<th>Suhu</th>
				</tr>
				<?php
				foreach ($catatan as $key => $value) { ?>
				<tr>
					<td><?= $value['tanggal']; ?></td>
					<td><?= $value['jam']; ?></td>
					<td><?= $value['lokasi']; ?></td>
					<td><?= $value['suhu']; ?></td>
				</tr>
				<?php } ?>
			</table>
		</div>
</body>
</html>