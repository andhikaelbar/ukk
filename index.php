<?php
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Peduli Diri</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<div class="head">
			<h1>Peduli Diri</h1>
			<p>Catatan Perjalanan</p>
		</div>
		<div class="nav">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="catatan.php">Catatan Perjalanan</a></li>
				<li><a href="input.php">Isi Data</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</div>
		<div class="body">
			<h1>Selamat Datang di Peduli Diri!</h1>
		</div>
	</div>
</body>
</html>