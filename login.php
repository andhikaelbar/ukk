<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="login form-login">
		<form method="POST" action="prosesLogin.php">
			<h2>Login</h2>
			<div class="row">
				<label for="nik">Nik</label>
				<input type="number" name="nik">
			</div>
			<div class="row">
				<label for="nama">Nama Lengkap</label>
				<input type="text" name="nama">
			</div>
			<input type="submit" value="Login">
			<p>Belum punya akun? <a href="register.php">Buat disini!</a></p>
		</form>
	</div>
</body>
</html>