<?php

$data = [
	'nik' => $_POST['nik'],
	'nama' => $_POST['nama']
];

$arr = file_get_contents("user.txt");
$user = json_decode($arr,true);

$login = false;
foreach ($user as $key => $value) {
	if ($data['nama'] == $value['nama'] && $data['nik'] == $value['nik']) {
		$login = true;
		break;
	}
}

if ($login) {
	session_start();
	$_SESSION['nik'] = $data['nik'];
	$_SESSION['nama'] = $data['nama'];
	header("Location: index.php");
} else {
	header("Location: login.php");
}

?>