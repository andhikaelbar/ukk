<?php
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Peduli Diri</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container">
		<div class="head">
			<h1>Peduli Diri</h1>
			<p>Catatan Perjalanan</p>
		</div>
		<div class="nav">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="catatan.php">Catatan Perjalanan</a></li>
				<li><a href="input.php">Isi Data</a></li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</div>
		<div class="form form-catatan">
			<form method="POST" action="simpanCatatan.php">
				<div class="row">
					<label for="tanggal">Tanggal</label>
					<input type="date" name="tanggal">
				</div>
				<div class="row">
					<label for="jam">Jam</label>
					<input type="time" name="jam">
				</div>
				<div class="row">
					<label for="lokasi">Lokasi yang Dikunjungi</label>
					<input type="textarea" name="lokasi">
				</div>
				<div class="row">
					<label for="suhu">Suhu</label>
					<input type="number" name="suhu">
				</div>
				<div class="btn btn-input">
					<input type="reset" value="Reset">
					<input type="submit" value="Submit">
				</div>
			</form>
		</div>
	</div>
</body>
</html>