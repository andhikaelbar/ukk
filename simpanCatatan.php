<?php

$data = [
	'tanggal' => $_POST['tanggal'],
	'jam' => $_POST['jam'],
	'lokasi' => $_POST['lokasi'],
	'suhu' => $_POST['suhu']
];
session_start();

$arr = file_get_contents($_SESSION['nik'].".txt");
$catatan = json_decode($arr,true);

array_push($catatan, $data);

$json_data = json_encode($catatan);
file_put_contents($_SESSION['nik'].".txt", $json_data);

header("Location: catatan.php");

?>